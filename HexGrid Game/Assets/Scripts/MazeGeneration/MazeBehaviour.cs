﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;
using UnityEngine.UI;

public class MazeBehaviour : MonoBehaviour
{

    [SerializeField]
    private int _mazeSize;
    [SerializeField]
    private int _roomSize;
    [SerializeField]
    private GameObject _prefab;
    [SerializeField]
    private GameEntity[] _maze;

    private List<RoomBehaviour> _roomBehaviours;
    [SerializeField]
    private string _seed;
    [SerializeField]
    private bool _useRandomSeed;

    private System.Random _pseudoRandom;

    public void Generate()
    {
        _seed = GetComponent<InputField>().text;
        GeneratePseudoRandom();
        _maze = MazeGenerator.Generate(transform, _mazeSize, _roomSize, _pseudoRandom);
    }

    private void GeneratePseudoRandom()
    {
        if (_useRandomSeed || _seed == null)
        {
            //Generating random _seed by using the system time
            _seed = System.DateTime.Now.Ticks.ToString();
            //Set _seed     
            _pseudoRandom = new System.Random(_seed.GetHashCode());
        }
        else
        {
            _pseudoRandom = new System.Random(_seed.GetHashCode());
        }
    }

}
