﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MazeGeneration;

public class MazeGenerator : MonoBehaviour
{

    public static GameEntity[] Generate(Transform parent, int mazeSize, int roomSize, System.Random pseudoRandom)
    {
        Contexts contexts = Contexts.sharedInstance;
        GameContext context = contexts.game;
        bool end = false;
        int mazeHeight = mazeSize * 2 + 1;
        float depth = 0.86602540378f * 1.732051f * (roomSize * 2 + 1);
        float width = 0.86602540378f * depth;

        GameEntity[] rooms = new GameEntity[mazeHeight * mazeHeight];

        GameObject container = new GameObject("Rooms");
        container.transform.SetParent(parent);

        for (int xIdx = -mazeSize; xIdx <= mazeSize; xIdx++)
        {
            for (int zIdx = -mazeSize; zIdx <= mazeSize; zIdx++)
            {
                float xPos = xIdx * width;
                float yPos = 0;
                float zPos = zIdx * depth;
                zPos += (depth / 2 - 0.75f) * xIdx;
                xPos += 0.86602540378f / 2 * xIdx + 0.86602540378f * zIdx;
                //flip room around x axis
                zPos *= -1;
                Vector3 position = new Vector3(xPos, yPos, zPos);

                GameEntity room;
                if (Mathf.Abs(-xIdx - zIdx) > mazeSize)
                {
                    //Disable unwanted rooms
                    room = context.CreateRoom(position, xIdx + mazeSize, -xIdx - zIdx + mazeSize, zIdx + mazeSize, roomSize,
                        (xIdx + mazeSize) * mazeHeight + (zIdx + mazeSize), pseudoRandom, false);
                }
                else
                {
                    room = context.CreateRoom(position, xIdx + mazeSize, -xIdx - zIdx + mazeSize, zIdx + mazeSize, roomSize,
                        (xIdx + mazeSize) * mazeHeight + (zIdx + mazeSize), pseudoRandom, true);
                }
                rooms[(xIdx + mazeSize) * mazeHeight + (zIdx + mazeSize)] = room;
            }
        }
        
        //change grid maze

        //backtracking -> depth
        Stack<GameEntity> backStack = new Stack<GameEntity>(); //1. backstack

        bool[] visited = new bool[rooms.Length];

        //start positie
        GameEntity start = rooms[mazeSize * mazeHeight + mazeSize];


        GameEntity current = start; //2a eerste element toevoegen
        backStack.Push(current); //2b
        visited[current.tilePosition.X * mazeHeight + current.tilePosition.Z] = true;

        while (backStack.Count > 0) // 3 loop doorheen de stack
        {
            // collect unvisited neighbours
            List<GameEntity> unvisitedNeighbours = new List<GameEntity>(6);

            if (current.tilePosition.X < (mazeSize * 2) &&
                !visited[(current.tilePosition.X + 1) * (mazeSize * 2 + 1) + current.tilePosition.Z] &&
                rooms[(current.tilePosition.X + 1) * (mazeSize * 2 + 1) + current.tilePosition.Z].enabled.Value)
                unvisitedNeighbours.Add(
                    rooms[(current.tilePosition.X + 1) * (mazeSize * 2 + 1) + current.tilePosition.Z]);

            if (current.tilePosition.X > 0 &&
                !visited[(current.tilePosition.X - 1) * (mazeSize * 2 + 1) + current.tilePosition.Z] &&
                rooms[(current.tilePosition.X - 1) * (mazeSize * 2 + 1) + current.tilePosition.Z].enabled.Value)
                unvisitedNeighbours.Add(
                    rooms[(current.tilePosition.X - 1) * (mazeSize * 2 + 1) + current.tilePosition.Z]);

            if (current.tilePosition.Z < (mazeSize * 2) &&
                !visited[current.tilePosition.X * (mazeSize * 2 + 1) + (current.tilePosition.Z + 1)] &&
                rooms[current.tilePosition.X * (mazeSize * 2 + 1) + (current.tilePosition.Z + 1)].enabled.Value)
                unvisitedNeighbours.Add(
                    rooms[current.tilePosition.X * (mazeSize * 2 + 1) + (current.tilePosition.Z + 1)]);

            if (current.tilePosition.Z > 0 &&
                !visited[current.tilePosition.X * (mazeSize * 2 + 1) + (current.tilePosition.Z - 1)] &&
                rooms[current.tilePosition.X * (mazeSize * 2 + 1) + (current.tilePosition.Z - 1)].enabled.Value)
                unvisitedNeighbours.Add(
                    rooms[current.tilePosition.X * (mazeSize * 2 + 1) + (current.tilePosition.Z - 1)]);

            if (current.tilePosition.X < (mazeSize * 2) && current.tilePosition.Z > 0 &&
                !visited[(current.tilePosition.X + 1) * (mazeSize * 2 + 1) + (current.tilePosition.Z - 1)] &&
                rooms[(current.tilePosition.X + 1) * (mazeSize * 2 + 1) + (current.tilePosition.Z - 1)].enabled
                    .Value)
                unvisitedNeighbours.Add(
                    rooms[(current.tilePosition.X + 1) * (mazeSize * 2 + 1) + (current.tilePosition.Z - 1)]);

            if (current.tilePosition.X > 0 && current.tilePosition.Z < (mazeSize * 2) &&
                !visited[(current.tilePosition.X - 1) * (mazeSize * 2 + 1) + (current.tilePosition.Z + 1)] &&
                rooms[(current.tilePosition.X - 1) * (mazeSize * 2 + 1) + (current.tilePosition.Z + 1)].enabled
                    .Value)
                unvisitedNeighbours.Add(
                    rooms[(current.tilePosition.X - 1) * (mazeSize * 2 + 1) + (current.tilePosition.Z + 1)]);


            if (unvisitedNeighbours.Count < 1) // 4a eindpunt bereikt op de backstack
            {
                if (!end)
                {
                    context.CreateEnd(backStack.Peek().startPosition.Value, true);
                    end = true;
                }

                backStack.Pop();
                if (backStack.Count > 0)
                {
                    current = backStack.Peek();
                }

            }
            else //4b we need to go deeper
            {
                GameEntity
                    neighbour = unvisitedNeighbours[
                        pseudoRandom.Next(0, unvisitedNeighbours.Count)]; // 5 kies een kind
                
                if (neighbour.tilePosition.X == current.tilePosition.X)
                {
                    neighbour.wallPositions.Positions &=
                        ~((neighbour.tilePosition.Z < current.tilePosition.Z)
                            ? WallPositions.Bottom
                            : WallPositions.Top);
                    current.wallPositions.Positions &=
                        ~((neighbour.tilePosition.Z < current.tilePosition.Z)
                            ? WallPositions.Top
                            : WallPositions.Bottom);

                }
                else if (neighbour.tilePosition.Y == current.tilePosition.Y)
                {
                    neighbour.wallPositions.Positions &=
                        ~((neighbour.tilePosition.X < current.tilePosition.X)
                            ? WallPositions.TopRight
                            : WallPositions.BottomLeft);
                    current.wallPositions.Positions &=
                        ~((neighbour.tilePosition.X < current.tilePosition.X)
                            ? WallPositions.BottomLeft
                            : WallPositions.TopRight);
                }
                else if (neighbour.tilePosition.Z == current.tilePosition.Z)
                {
                    neighbour.wallPositions.Positions &=
                        ~((neighbour.tilePosition.X < current.tilePosition.X)
                            ? WallPositions.BottomRight
                            : WallPositions.TopLeft);
                    current.wallPositions.Positions &=
                        ~((neighbour.tilePosition.X < current.tilePosition.X)
                            ? WallPositions.TopLeft
                            : WallPositions.BottomRight);
                }
                
                current = neighbour; //6a bouw de backstack op
                backStack.Push(current); //6b
                visited[current.tilePosition.X * mazeHeight + current.tilePosition.Z] = true;
            }
        }
        return rooms;
    }
}






