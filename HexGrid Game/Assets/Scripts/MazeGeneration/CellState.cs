﻿namespace MazeGeneration
{
    public enum TileState
    {
        Walkable = 1,
        Wall = 2,
        Solid = 4,
        Path = 8,
    }

}
