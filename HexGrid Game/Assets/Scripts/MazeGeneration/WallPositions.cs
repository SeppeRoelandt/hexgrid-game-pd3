﻿using System;

namespace MazeGeneration
{
        [Flags]
        public enum WallPositions
        {
            Top = 1,
            TopLeft = 2,
            TopRight = 4,
            Bottom = 8,
            BottomLeft = 16,
            BottomRight = 32,
        }
}