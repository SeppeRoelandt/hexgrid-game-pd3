﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGenerator : MonoBehaviour
{
    public static GameEntity[] Generate(Transform transform, int size, int roomId)
    {
        Contexts contexts = Contexts.sharedInstance;
        GameContext context = contexts.game;

        int roomSize = size;
        int roomHeight = roomSize * 2 + 1;
        float width = 1.732051f;
        float height = 1.732051f;
        float depth = 0.86602540378f * width;

        GameEntity[] cells = new GameEntity[roomHeight * roomHeight];

        for (int xIdx = -roomSize; xIdx <= roomSize; xIdx++)
        {
            for (int zIdx = -roomSize; zIdx <= roomSize; zIdx++)
            {
                float xPos = transform.position.x + xIdx * width;
                float yPos = transform.position.y;
                float zPos = transform.position.z + (zIdx * depth)*-1;
                //x offset 
                xPos += 0.86602540378f * zIdx;
                //flip room around x axis
                //zPos *= -1;
                Vector3 position = new Vector3(xPos, yPos, zPos);

                GameEntity tile;
                if (Mathf.Abs(-xIdx - zIdx) > roomSize)
                {
                    //Disable unwanted cells
                    tile = context.CreateTile(position, xIdx + roomSize, -xIdx - zIdx + roomSize, zIdx + roomSize,
                        (int) ((xIdx + roomSize) * roomHeight + (zIdx + roomSize)), roomId, 10, false);
                    tile.AddTileState(MazeGeneration.TileState.Solid);
                }
                else
                {
                    tile = context.CreateTile(position, xIdx + roomSize, -xIdx - zIdx + roomSize, zIdx + roomSize,
                        (int) ((xIdx + roomSize) * roomHeight + (zIdx + roomSize)), roomId, 1, true);        
                    tile.AddTileState(MazeGeneration.TileState.Walkable);
                }
                cells[(xIdx + roomSize) * roomHeight + (zIdx + roomSize)] = tile;
            }
        }
        return cells;
    }
}


