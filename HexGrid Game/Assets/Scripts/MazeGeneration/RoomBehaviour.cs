﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using General;
using MazeGeneration;
using Entitas.Unity;

public class RoomBehaviour : MonoBehaviour
{
    private GameEntity _entity;

    private GameEntity[] _room;
    private int _size = 10;
    private WallPositions _wallPositions;
    private int _roomId;
    private int _randomFillPercent;
    private int _smoothingLvl;
    private System.Random _pseudoRandom;

    private readonly List<GameEntity> _cellsToConnect = new List<GameEntity>(6);

    void Start()
    {
        _entity = gameObject.GetEntityLink().entity as GameEntity;

        _size = _entity.roomSize.Value;
        _wallPositions = _entity.wallPositions.Positions;
        _roomId = _entity.roomId.Value;
        _pseudoRandom = _entity.pseudoRandom.PseudoRandom;

        if (_entity.enabled.Value)
        {
            Generate();
        }
    }

    public void Generate()
    {
        _randomFillPercent = _pseudoRandom.Next(0, 40) + _pseudoRandom.Next(15, 45);
        _smoothingLvl = _pseudoRandom.Next(1, 2);

        _room = RoomGenerator.Generate(transform, _size, _roomId);
        RandomFillRoom();
        ConnectAllEntrances();
        for (int i = 0; i < _smoothingLvl; i++)
        {
            SmoothRoom();
        }
        ScaleRoomTriggerWithSize();

        foreach (var tile in _room)
        {
            if (tile.enabled.Value)
            {
                tile.isTile = true;
            }
        }
        _entity.AddRoomTiles(_room);
        Destroy(this);
    }

    private void RandomFillRoom()
    {
        //Fill room
        for (int x = 0; x < _size * 2 + 1; x++)
        {
            for (int z = 0; z < _size * 2 + 1; z++)
            {
                //Fill room random
                if (_room[x * (_size * 2 + 1) + z].enabled.Value)
                {
                    //_room[x * (_size * 2 + 1) + z].AddCellState(CellState.State.Walkable);
                    if (_pseudoRandom.Next(0, 100) < _randomFillPercent)
                    {
                        _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Walkable;
                    }
                    else
                    {
                        _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Wall;
                    }
                    //Set walls
                    SetWalls(x, z);
                }
            }
        }
    }

    private void SetWalls(int x, int z)
    {
        //Set all borders
        //Top                
        if (_room[x * (_size * 2 + 1) + z].tilePosition.Z - _size <= -_size)
        {
            if ((x > _size + (_size / 2) + 1 || x < _size + _size / 2 - 1) ||
                (_wallPositions & WallPositions.Top) == WallPositions.Top)
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Solid;
            }
            else
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Path;

                //Connect doorway middle cell to cells to connect
                if (x == _size + _size / 2)
                {
                    _cellsToConnect.Add(_room[x * (_size * 2 + 1) + z]);
                }
            }
        }

        //TopLeft
        if (_room[x * (_size * 2 + 1) + z].tilePosition.Y - _size >= _size)
        {
            if ((x > (_size / 2) + 1 || x < _size / 2 - 1 ||
                 z > (_size / 2) + 1 || z < _size / 2 - 1) ||
                (_wallPositions & WallPositions.TopLeft) == WallPositions.TopLeft)
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Solid;
            }
            else
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Path;
                //Connect doorway middle cell to cells to connect
                if (x == _size / 2 && z == _size / 2)
                {
                    _cellsToConnect.Add(_room[x * (_size * 2 + 1) + z]);
                }
            }
        }

        //TopRight
        if (_room[x * (_size * 2 + 1) + z].tilePosition.X - _size >= _size)
        {
            if ((z > (_size / 2) + 1 || z < _size / 2 - 1) ||
                (_wallPositions & WallPositions.TopRight) == WallPositions.TopRight)
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Solid;
            }
            else
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Path;
                //Connect doorway middle cell to cells to connect
                if (z == _size / 2)
                {
                    _cellsToConnect.Add(_room[x * (_size * 2 + 1) + z]);
                }
            }
        }
        //Bottom
        if (_room[x * (_size * 2 + 1) + z].tilePosition.Z - _size >= _size)
        {
            if ((x > (_size / 2) + 1 || x < _size / 2 - 1) ||
                (_wallPositions & WallPositions.Bottom) == WallPositions.Bottom)
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Solid;
            }
            else
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Path;
                //Connect doorway middle cell to cells to connect
                if (x == _size / 2)
                {
                    _cellsToConnect.Add(_room[x * (_size * 2 + 1) + z]);
                }
            }
        }
        //BottomRight
        if (_room[x * (_size * 2 + 1) + z].tilePosition.Y - _size <= -_size)
        {
            if ((x > _size + (_size / 2) + 1 || x < _size + _size / 2 - 1 ||
                 z > _size + (_size / 2) + 1 || z < _size + _size / 2 - 1) ||
                (_wallPositions & WallPositions.BottomRight) == WallPositions.BottomRight)
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Solid;
            }
            else
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Path;
                //Connect doorway middle cell to cells to connect
                if (x == _size + _size / 2 && x == _size + _size / 2)
                {
                    _cellsToConnect.Add(_room[x * (_size * 2 + 1) + z]);
                }
            }
        }

        //BottomLeft
        if (_room[x * (_size * 2 + 1) + z].tilePosition.X - _size <= -_size)
        {
            if ((z > _size + (_size / 2) + 1 || z < _size + _size / 2 - 1) ||
                (_wallPositions & WallPositions.BottomLeft) == WallPositions.BottomLeft)
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Solid;
            }
            else
            {
                _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Path;
                //Connect doorway middle cell to cells to connect
                if (z == _size + _size / 2)
                {
                    _cellsToConnect.Add(_room[x * (_size * 2 + 1) + z]);
                }
            }
        }
    }

    private void SmoothRoom()
    {
        //Smooth the room
        for (int x = 0; x < _size * 2 + 1; x++)
        {
            for (int z = 0; z < _size * 2 + 1; z++)
            {
                //cell is enabled
                if (_room[x * (_size * 2 + 1) + z].enabled.Value &&
                    (_room[x * (_size * 2 + 1) + z].tilePosition.Y != 0 || _room[x * (_size * 2 + 1) + z].tilePosition.Y != _size * 2) &&
                    (_room[x * (_size * 2 + 1) + z].tileState.State != TileState.Solid))
                {
                    int neighbourWallCells = GetSurroundingWallCount(_room[x * (_size * 2 + 1) + z]);
                    if (_room[x * (_size * 2 + 1) + z].tileState.State == TileState.Path)
                    {
                        _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Walkable;
                    }
                    else if (neighbourWallCells > 3)
                    {
                        _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Wall;
                    }
                    else if (neighbourWallCells < 3)
                    {
                        _room[x * (_size * 2 + 1) + z].tileState.State = TileState.Walkable;
                    }
                }
                else if (_room[x * (_size * 2 + 1) + z].enabled.Value)
                {
                    //Set walls
                    SetWalls(x, z);
                }
            }
        }
    }

    private List<GameEntity> GetNeighbours(GameEntity current)
    {
        List<GameEntity> neighbours = new List<GameEntity>(6);

        if (current.tilePosition.X < (_size * 2) &&
            _room[(current.tilePosition.X + 1) * (_size * 2 + 1) + current.tilePosition.Z].enabled.Value)
            neighbours.Add(_room[(current.tilePosition.X + 1) * (_size * 2 + 1) + current.tilePosition.Z]);

        if (current.tilePosition.X > 0 &&
            _room[(current.tilePosition.X - 1) * (_size * 2 + 1) + current.tilePosition.Z].enabled.Value)
            neighbours.Add(_room[(current.tilePosition.X - 1) * (_size * 2 + 1) + current.tilePosition.Z]);

        if (current.tilePosition.Z < (_size * 2) &&
            _room[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z + 1)].enabled.Value)
            neighbours.Add(_room[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z + 1)]);

        if (current.tilePosition.Z > 0 &&
            _room[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z - 1)].enabled.Value)
            neighbours.Add(_room[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z - 1)]);

        if (current.tilePosition.X < (_size * 2) && current.tilePosition.Z > 0 &&
            _room[(current.tilePosition.X + 1) * (_size * 2 + 1) + (current.tilePosition.Z - 1)].enabled.Value)
            neighbours.Add(_room[(current.tilePosition.X + 1) * (_size * 2 + 1) + (current.tilePosition.Z - 1)]);

        if (current.tilePosition.X > 0 && current.tilePosition.Z < (_size * 2) &&
            _room[(current.tilePosition.X - 1) * (_size * 2 + 1) + (current.tilePosition.Z + 1)].enabled.Value)
            neighbours.Add(_room[(current.tilePosition.X - 1) * (_size * 2 + 1) + (current.tilePosition.Z + 1)]);
        return neighbours;
    }

    private int GetSurroundingWallCount(GameEntity current)
    {
        int wallCount = 0;
        List<GameEntity> neighbours = GetNeighbours(current);
        foreach (var neighbour in neighbours)
        {
            if (neighbour.tileState.State == TileState.Wall || neighbour.tileState.State == TileState.Solid)
            {
                wallCount++;
            }
            if (neighbour.tileState.State == TileState.Path)
            {
                return 0;
            }
        }
        wallCount += 6 - neighbours.Count; 
        return wallCount;
    }

    public void FindAndCreatePath(GameEntity startcell, GameEntity endcell)
    {
        Dictionary<GameEntity, int> pathValues = new Dictionary<GameEntity, int>();

        PriorityQueue<GameEntity> queue = new PriorityQueue<GameEntity>((left, right) =>
        {
            //manhattan
            var distance1 =
                Mathf.Max(Mathf.Abs(left.tilePosition.X - endcell.tilePosition.X) +
                          Mathf.Abs(left.tilePosition.Y - endcell.tilePosition.Y) +
                          Mathf.Abs(left.tilePosition.Z - endcell.tilePosition.Z)) / 2 + left.penalty.Value;
            var distance2 =
                Mathf.Max(Mathf.Abs(right.tilePosition.X - endcell.tilePosition.X) +
                          Mathf.Abs(right.tilePosition.Y - endcell.tilePosition.Y) +
                          Mathf.Abs(right.tilePosition.Z - endcell.tilePosition.Z)) / 2 + right.penalty.Value;

            return distance1.CompareTo(distance2);
        });

        Dictionary<GameEntity, GameEntity> cameFrom = new Dictionary<GameEntity, GameEntity>();


        bool[] visited = new bool[_room.Length];

        //Start van de start positie
        GameEntity start = startcell;
        queue.Enqueue(start);
        pathValues[start] = 0;

        bool found = false;
        while (queue.Count > 0 && !found)
        {
            queue.IsConsistent();
            GameEntity current = queue.Dequeue();

            //early exit
            if (current == endcell)
            {
                found = true;
            }

            List<GameEntity> unvisitedNeighbours = FilterNeighbours(current, visited);
            foreach (var neighbour in unvisitedNeighbours)
            {
                cameFrom[neighbour] = current;

                pathValues[neighbour] = pathValues[current] + neighbour.penalty.Value;
                queue.Enqueue(neighbour);
            }

            visited[current.tilePosition.X * (_size * 2 + 1) + current.tilePosition.Z] = true;
        }

        GameEntity pathPart = endcell;
        while (pathPart != startcell)
        {
            pathPart.tileState.State = TileState.Path;
            pathPart = cameFrom[pathPart];
        }
    }

    private List<GameEntity> FilterNeighbours(GameEntity current, bool[] visited)
    {
        // collect unvisited neighbours
        List<GameEntity> unvisitedNeighbours = new List<GameEntity>(6);

        if (current.tilePosition.X < (_size * 2) && !visited[(current.tilePosition.X + 1) * (_size * 2 + 1) + current.tilePosition.Z] &&
            _room[(current.tilePosition.X + 1) * (_size * 2 + 1) + current.tilePosition.Z].enabled.Value)
            unvisitedNeighbours.Add(_room[(current.tilePosition.X + 1) * (_size * 2 + 1) + current.tilePosition.Z]);

        if (current.tilePosition.X > 0 && !visited[(current.tilePosition.X - 1) * (_size * 2 + 1) + current.tilePosition.Z] &&
            _room[(current.tilePosition.X - 1) * (_size * 2 + 1) + current.tilePosition.Z].enabled.Value)
            unvisitedNeighbours.Add(_room[(current.tilePosition.X - 1) * (_size * 2 + 1) + current.tilePosition.Z]);

        if (current.tilePosition.Z < (_size * 2) && !visited[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z + 1)] &&
            _room[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z + 1)].enabled.Value)
            unvisitedNeighbours.Add(_room[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z + 1)]);

        if (current.tilePosition.Z > 0 && !visited[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z - 1)] &&
            _room[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z - 1)].enabled.Value)
            unvisitedNeighbours.Add(_room[current.tilePosition.X * (_size * 2 + 1) + (current.tilePosition.Z - 1)]);

        if (current.tilePosition.X < (_size * 2) && current.tilePosition.Z > 0 && !visited[(current.tilePosition.X + 1) * (_size * 2 + 1) + (current.tilePosition.Z - 1)] &&
            _room[(current.tilePosition.X + 1) * (_size * 2 + 1) + (current.tilePosition.Z - 1)].enabled.Value)
            unvisitedNeighbours.Add(_room[(current.tilePosition.X + 1) * (_size * 2 + 1) + (current.tilePosition.Z - 1)]);

        if (current.tilePosition.X > 0 && current.tilePosition.Z < (_size * 2) && !visited[(current.tilePosition.X - 1) * (_size * 2 + 1) + (current.tilePosition.Z + 1)] &&
            _room[(current.tilePosition.X - 1) * (_size * 2 + 1) + (current.tilePosition.Z + 1)].enabled.Value)
            unvisitedNeighbours.Add(_room[(current.tilePosition.X - 1) * (_size * 2 + 1) + (current.tilePosition.Z + 1)]);
    

    return unvisitedNeighbours;
    }

    private void ConnectAllEntrances()
    {
        {
            if (_cellsToConnect.Count == 1)
            {
                _cellsToConnect.Add(_room[_size * (_size * 2 + 1) + _size]);
            }

            for (var i = 0; i < _cellsToConnect.Count - 1; i++)
            {
                FindAndCreatePath(_cellsToConnect[i], _cellsToConnect[i + 1]);
            }
        }
    }

    private void ScaleRoomTriggerWithSize()
    {
        gameObject.transform.localScale = Vector3.one * 0.3f + Vector3.one * _size * 1.732051f;
    }
}
