﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGenerator : MonoBehaviour
{
    public static RoomBehaviour[] Generate(Transform parent, int mazeSize, int roomSize, System.Random pseudoRandom)
    {
        Contexts contexts = Contexts.sharedInstance;
        GameContext context = contexts.game;

        int mazeHeight = mazeSize * 2 + 1;
        float depth = 0.86602540378f * 1.732051f * (roomSize * 2 + 1);
        float width = 0.86602540378f * depth;

        RoomBehaviour[] rooms = new RoomBehaviour[mazeHeight * mazeHeight];

        GameObject container = new GameObject("Rooms");
        container.transform.SetParent(parent);

        for (int xIdx = -mazeSize; xIdx <= mazeSize; xIdx++)
        {
            for (int zIdx = -mazeSize; zIdx <= mazeSize; zIdx++)
            {
                float xPos = xIdx * width;
                float yPos = 0;
                float zPos = zIdx * depth;
                zPos += (depth / 2 - 0.75f) * xIdx;
                xPos += 0.86602540378f / 2 * xIdx + 0.86602540378f * zIdx;
                //flip room around x axis
                zPos *= -1;
                Vector3 position = new Vector3(xPos, yPos, zPos);
                GameEntity room;
                if (Mathf.Abs(-xIdx - zIdx) > mazeSize)
                {
                    //Disable unwanted rooms
                    room = context.CreateTile(position, xIdx + roomSize, -xIdx - zIdx + roomSize, zIdx + roomSize, 10, roomId, false);
                    room.AddTileState(MazeGeneration.CellState.State.Solid);
                }
                else
                {
                    room = context.CreateTile(position, xIdx + roomSize, -xIdx - zIdx + roomSize, zIdx + roomSize, 1, roomId, true);
                    room.AddTileState(MazeGeneration.CellState.State.Walkable);
                }
        }

        /*
        //change grid maze

        //backtracking -> depth
        Stack<RoomBehaviour> backStack = new Stack<RoomBehaviour>(); //1. backstack

        bool[] visited = new bool[rooms.Length];

        //start positie
        RoomBehaviour start = rooms[mazeSize * mazeHeight + mazeSize];


        RoomBehaviour current = start; //2a eerste element toevoegen
        backStack.Push(current); //2b
        visited[current.X * mazeHeight + current.Z] = true;

        while (backStack.Count > 0) // 3 loop doorheen de stack
        {
            // collect unvisited neighbours
            List<RoomBehaviour> unvisitedNeighbours = new List<RoomBehaviour>(6);

            if (current.X < (mazeSize * 2) && !visited[(current.X + 1) * (mazeSize * 2 + 1) + current.Z] &&
                rooms[(current.X + 1) * (mazeSize * 2 + 1) + current.Z].enabled)
                unvisitedNeighbours.Add(rooms[(current.X + 1) * (mazeSize * 2 + 1) + current.Z]);

            if (current.X > 0 && !visited[(current.X - 1) * (mazeSize * 2 + 1) + current.Z] &&
                rooms[(current.X - 1) * (mazeSize * 2 + 1) + current.Z].enabled)
                unvisitedNeighbours.Add(rooms[(current.X - 1) * (mazeSize * 2 + 1) + current.Z]);

            if (current.Z < (mazeSize * 2) && !visited[current.X * (mazeSize * 2 + 1) + (current.Z + 1)] &&
                rooms[current.X * (mazeSize * 2 + 1) + (current.Z + 1)].enabled)
                unvisitedNeighbours.Add(rooms[current.X * (mazeSize * 2 + 1) + (current.Z + 1)]);

            if (current.Z > 0 && !visited[current.X * (mazeSize * 2 + 1) + (current.Z - 1)] &&
                rooms[current.X * (mazeSize * 2 + 1) + (current.Z - 1)].enabled)
                unvisitedNeighbours.Add(rooms[current.X * (mazeSize * 2 + 1) + (current.Z - 1)]);

            if (current.X < (mazeSize * 2) && current.Z > 0 && !visited[(current.X + 1) * (mazeSize * 2 + 1) + (current.Z - 1)] &&
                rooms[(current.X + 1) * (mazeSize * 2 + 1) + (current.Z - 1)].enabled)
                unvisitedNeighbours.Add(rooms[(current.X + 1) * (mazeSize * 2 + 1) + (current.Z - 1)]);

            if (current.X > 0 && current.Z < (mazeSize * 2) && !visited[(current.X - 1) * (mazeSize * 2 + 1) + (current.Z + 1)] &&
                rooms[(current.X - 1) * (mazeSize * 2 + 1) + (current.Z + 1)].enabled)
                unvisitedNeighbours.Add(rooms[(current.X - 1) * (mazeSize * 2 + 1) + (current.Z + 1)]);


            if (unvisitedNeighbours.Count < 1) // 4a eindpunt bereikt op de backstack
            {
                backStack.Pop();
                if (backStack.Count > 0)
                    current = backStack.Peek();
            }
            else //4b we need to go deeper
            {
                RoomBehaviour
                    neighbour = unvisitedNeighbours[pseudoRandom.Next(0, unvisitedNeighbours.Count)]; // 5 kies een kind

                //Destroy Walls
                if (neighbour.X == current.X)
                {
                    neighbour.WallPositions &=
                        ~((neighbour.Z < current.Z) ? WallPositions.Bottom : WallPositions.Top);
                    current.WallPositions &=
                        ~((neighbour.Z < current.Z) ? WallPositions.Top : WallPositions.Bottom);
                        
                }
                else if (neighbour.Y == current.Y)
                {
                    neighbour.WallPositions &=
                        ~((neighbour.X < current.X) ? WallPositions.TopRight : WallPositions.BottomLeft);
                    current.WallPositions &=
                        ~((neighbour.X < current.X) ? WallPositions.BottomLeft : WallPositions.TopRight);
                }
                else if (neighbour.Z == current.Z)
                {
                    neighbour.WallPositions &=
                        ~((neighbour.X < current.X) ? WallPositions.BottomRight : WallPositions.TopLeft);
                    current.WallPositions &=
                        ~((neighbour.X < current.X) ? WallPositions.TopLeft : WallPositions.BottomRight);
                }

                current = neighbour; //6a bouw de backstack op
                backStack.Push(current); //6b
                visited[current.X * mazeHeight + current.Z] = true;
            }
        }
        
        return rooms;
    }
}




