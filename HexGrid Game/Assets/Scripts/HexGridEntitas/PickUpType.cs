﻿using System;

namespace PickUp
{
    public enum PickUpType
    {
        ProjectileSpeedIncrease = 1,
        ProjectileSizeIncrease = 2,
        ProjectileDamageIncrease = 4,
        MovementSpeedIncrease = 8,
        Heal = 16,
    }
}