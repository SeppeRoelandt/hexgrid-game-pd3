﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class HitComponent : IComponent
    {
        public float Value;
    }
}
