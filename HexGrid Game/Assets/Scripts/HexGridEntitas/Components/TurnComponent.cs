﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    [Game]
    public class TurnComponent : IComponent
    {
        public Vector2 Amount;
        public float DeltaTime;
    }
}