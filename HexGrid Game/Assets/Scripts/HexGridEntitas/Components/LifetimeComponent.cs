﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class LifetimeComponent : IComponent
    {
        public float Value;
    }
}
