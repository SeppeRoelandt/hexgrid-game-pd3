﻿using Entitas;
using MazeGeneration;

namespace HexGridEntitas.Components
{
    [Game]
    public class WallPositionsComponent : IComponent
    {
        public WallPositions Positions;
    }
}
