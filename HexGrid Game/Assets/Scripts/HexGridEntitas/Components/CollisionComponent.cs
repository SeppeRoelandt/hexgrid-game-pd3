﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    public class CollisionComponent : IComponent
    {
        public Collider Other;
    }
}