﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class RoomSizeComponent : IComponent
    {
        public int Value;
    }
}
