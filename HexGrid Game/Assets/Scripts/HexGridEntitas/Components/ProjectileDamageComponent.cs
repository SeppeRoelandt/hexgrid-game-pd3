﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class ProjectileDamageComponent : IComponent
    {
        public float Value;
    }
}
