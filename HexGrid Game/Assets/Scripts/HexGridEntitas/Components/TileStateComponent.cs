﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class TileStateComponent : IComponent
    {
        public MazeGeneration.TileState State;
    }
}
