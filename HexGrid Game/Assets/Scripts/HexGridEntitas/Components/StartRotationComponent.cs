﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    [Game]
    public class StartRotationComponent : IComponent
    {
        public Quaternion Value;
    }
}
