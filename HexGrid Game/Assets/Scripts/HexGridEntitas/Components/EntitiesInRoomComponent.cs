﻿using System.Collections.Generic;
using Entitas;
namespace HexGridEntitas.Components
{
    [Game]
    public class EntitiesInRoomComponent : IComponent
    {
        public List<GameEntity> Entities;
    }
}
