﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    [Game]
    public class TilePositionComponent : IComponent
    {
        public int X;
        public int Y;
        public int Z;
    }
}
