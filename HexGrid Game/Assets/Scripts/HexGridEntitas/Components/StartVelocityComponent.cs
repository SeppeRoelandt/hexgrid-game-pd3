﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    [Game]
    public class StartVelocityComponent : IComponent
    {
        public Vector3 Value;
    }
}