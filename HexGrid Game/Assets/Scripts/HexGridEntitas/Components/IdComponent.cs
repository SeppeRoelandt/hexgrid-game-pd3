﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class IdComponent : IComponent
    {
        public int Value;
    }
}
