﻿using Entitas;
namespace HexGridEntitas.Components
{
    [Game]
    public class EnabledComponent : IComponent
    {
        public bool Value;
    }
}
