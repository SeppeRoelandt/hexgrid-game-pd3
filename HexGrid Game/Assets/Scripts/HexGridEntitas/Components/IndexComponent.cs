﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
public class IndexComponent : IComponent
{
    [EntityIndex]
    public string Index;
}
// Can now use Game.GetEntitiesWithFaction("Player");