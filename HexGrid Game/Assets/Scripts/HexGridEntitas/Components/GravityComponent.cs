﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class GravityComponent : IComponent
    {
        public float Value;
    }
}
