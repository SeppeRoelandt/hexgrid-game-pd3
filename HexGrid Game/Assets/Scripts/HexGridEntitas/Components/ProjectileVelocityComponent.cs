﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    [Game]
    public class ProjectileVelocityComponent : IComponent
    {
        public int Value;
    }
}