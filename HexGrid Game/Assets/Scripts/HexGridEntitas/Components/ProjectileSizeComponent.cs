﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    [Game]
    public class ProjectileSizeComponent : IComponent
    {
        public Vector3 Value;
    }
}
