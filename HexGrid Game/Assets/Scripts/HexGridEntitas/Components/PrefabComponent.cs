﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class PrefabComponent : IComponent
    {
        public string Value;
    }
}