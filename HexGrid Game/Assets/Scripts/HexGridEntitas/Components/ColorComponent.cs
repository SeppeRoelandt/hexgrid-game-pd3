﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    [Game]
    public class ColorComponent : IComponent
    {
        public Color Value;
    }
}
