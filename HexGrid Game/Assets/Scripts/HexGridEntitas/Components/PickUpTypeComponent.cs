﻿using Entitas;
using PickUp;

namespace HexGridEntitas.Components
{
    [Game]
    public class PickUpTypeComponent : IComponent
    {
        public PickUpType Type;
    }
}
