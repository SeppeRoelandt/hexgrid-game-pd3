﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace HexGridEntitas.Components
{
    [Game]
    public class RoomIdComponent : IComponent
    {
        [EntityIndex]
        public int Value;
    }
}
