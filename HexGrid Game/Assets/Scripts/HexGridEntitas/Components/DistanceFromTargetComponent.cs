﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class DistanceFromTargetComponent : IComponent
    {
        public int Value;
    }
}
