﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    [Game]
    public class StartPositionComponent : IComponent
    {
        public Vector3 Value;
    }
}
