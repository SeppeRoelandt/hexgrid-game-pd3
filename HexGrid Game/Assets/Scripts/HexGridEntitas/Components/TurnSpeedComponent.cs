﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class TurnSpeedComponent : IComponent
    {
        public float Value;
    }
}
