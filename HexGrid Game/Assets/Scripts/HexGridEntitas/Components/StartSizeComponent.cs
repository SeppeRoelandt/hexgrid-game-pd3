﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Components
{
    [Game]
    public class StartSizeComponent : IComponent
    {
        public Vector3 Value;
    }
}
