﻿using System.Collections.Generic;
using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class PathToTargetComponent : IComponent
    {
        public List<GameEntity> Tiles;
    }
}
