﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class RoomTilesComponent : IComponent
    {
        public GameEntity[] Tiles;
    }
}
