﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class MoveSpeedComponent : IComponent
    {
        public float Value;
    }
}
