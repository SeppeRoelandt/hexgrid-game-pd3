﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class TargetComponent : IComponent
    {
        public GameEntity Target;
    }
}