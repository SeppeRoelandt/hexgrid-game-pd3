﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class HealthComponent : IComponent
    {
        public float Value;
        public float MaxValue;
    }
}
