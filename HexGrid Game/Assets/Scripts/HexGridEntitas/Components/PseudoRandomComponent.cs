﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class PseudoRandomComponent : IComponent
    {
        public System.Random PseudoRandom;
    }
}
