﻿using Entitas;

namespace HexGridEntitas.Components
{
    [Game]
    public class FireRateComponent : IComponent
    {
        public float Value;
        public float TimeOfLastShot;
    }
}
