﻿using UnityEngine;
using UnityEngine.UI;

namespace HexGridEntitas.Views
{
    public class HealthVisualizer : MonoBehaviour
    {
        [SerializeField]
        private Slider _slider;

        public float Health
        {
            set { _slider.value = value; }
        }


    }
}