﻿using UnityEngine;

namespace HexGridEntitas.Views
{
    public class Colorized : MonoBehaviour
    {
        public Color Color
        {
            set
            {
                foreach (MeshRenderer renderer in gameObject.GetComponentsInChildren<MeshRenderer>())
                {
                    renderer.material.color = value;
                }
            }
        }
    }
}
