﻿using Entitas.Unity;
using UnityEngine;


namespace HexGridEntitas.Views
{
    public class Trigger : MonoBehaviour
    {
        void OnTriggerEnter(Collider other)
        {
            GameEntity entity = gameObject.GetEntityLink().entity as GameEntity;
            if (entity.hasCollision)
            {
                entity.ReplaceCollision(other);
            }
            else
            {
                entity.AddCollision(other);
            }
        }
    }
}

