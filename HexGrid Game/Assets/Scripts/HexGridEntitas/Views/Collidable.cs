﻿using Entitas.Unity;
using UnityEngine;


namespace HexGridEntitas.Views
{
    public class Collidable : MonoBehaviour
    {
        void OnCollisionEnter(Collision other)
        {
            GameEntity entity = gameObject.GetEntityLink().entity as GameEntity;
            if (!entity.hasCollision)
            {
                entity.AddCollision(other.collider);
            }
        }
    }
}
