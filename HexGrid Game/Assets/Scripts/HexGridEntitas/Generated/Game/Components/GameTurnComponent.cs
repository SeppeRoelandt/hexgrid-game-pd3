//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public HexGridEntitas.Components.TurnComponent turn { get { return (HexGridEntitas.Components.TurnComponent)GetComponent(GameComponentsLookup.Turn); } }
    public bool hasTurn { get { return HasComponent(GameComponentsLookup.Turn); } }

    public void AddTurn(UnityEngine.Vector2 newAmount, float newDeltaTime) {
        var index = GameComponentsLookup.Turn;
        var component = CreateComponent<HexGridEntitas.Components.TurnComponent>(index);
        component.Amount = newAmount;
        component.DeltaTime = newDeltaTime;
        AddComponent(index, component);
    }

    public void ReplaceTurn(UnityEngine.Vector2 newAmount, float newDeltaTime) {
        var index = GameComponentsLookup.Turn;
        var component = CreateComponent<HexGridEntitas.Components.TurnComponent>(index);
        component.Amount = newAmount;
        component.DeltaTime = newDeltaTime;
        ReplaceComponent(index, component);
    }

    public void RemoveTurn() {
        RemoveComponent(GameComponentsLookup.Turn);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherTurn;

    public static Entitas.IMatcher<GameEntity> Turn {
        get {
            if (_matcherTurn == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.Turn);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherTurn = matcher;
            }

            return _matcherTurn;
        }
    }
}
