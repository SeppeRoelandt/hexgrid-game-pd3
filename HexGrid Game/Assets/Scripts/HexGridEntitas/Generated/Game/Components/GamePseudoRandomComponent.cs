//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public HexGridEntitas.Components.PseudoRandomComponent pseudoRandom { get { return (HexGridEntitas.Components.PseudoRandomComponent)GetComponent(GameComponentsLookup.PseudoRandom); } }
    public bool hasPseudoRandom { get { return HasComponent(GameComponentsLookup.PseudoRandom); } }

    public void AddPseudoRandom(System.Random newPseudoRandom) {
        var index = GameComponentsLookup.PseudoRandom;
        var component = CreateComponent<HexGridEntitas.Components.PseudoRandomComponent>(index);
        component.PseudoRandom = newPseudoRandom;
        AddComponent(index, component);
    }

    public void ReplacePseudoRandom(System.Random newPseudoRandom) {
        var index = GameComponentsLookup.PseudoRandom;
        var component = CreateComponent<HexGridEntitas.Components.PseudoRandomComponent>(index);
        component.PseudoRandom = newPseudoRandom;
        ReplaceComponent(index, component);
    }

    public void RemovePseudoRandom() {
        RemoveComponent(GameComponentsLookup.PseudoRandom);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPseudoRandom;

    public static Entitas.IMatcher<GameEntity> PseudoRandom {
        get {
            if (_matcherPseudoRandom == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.PseudoRandom);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPseudoRandom = matcher;
            }

            return _matcherPseudoRandom;
        }
    }
}
