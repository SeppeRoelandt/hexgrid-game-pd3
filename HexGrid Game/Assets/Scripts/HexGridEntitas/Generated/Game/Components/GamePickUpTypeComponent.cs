//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public HexGridEntitas.Components.PickUpTypeComponent pickUpType { get { return (HexGridEntitas.Components.PickUpTypeComponent)GetComponent(GameComponentsLookup.PickUpType); } }
    public bool hasPickUpType { get { return HasComponent(GameComponentsLookup.PickUpType); } }

    public void AddPickUpType(PickUp.PickUpType newType) {
        var index = GameComponentsLookup.PickUpType;
        var component = CreateComponent<HexGridEntitas.Components.PickUpTypeComponent>(index);
        component.Type = newType;
        AddComponent(index, component);
    }

    public void ReplacePickUpType(PickUp.PickUpType newType) {
        var index = GameComponentsLookup.PickUpType;
        var component = CreateComponent<HexGridEntitas.Components.PickUpTypeComponent>(index);
        component.Type = newType;
        ReplaceComponent(index, component);
    }

    public void RemovePickUpType() {
        RemoveComponent(GameComponentsLookup.PickUpType);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPickUpType;

    public static Entitas.IMatcher<GameEntity> PickUpType {
        get {
            if (_matcherPickUpType == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.PickUpType);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPickUpType = matcher;
            }

            return _matcherPickUpType;
        }
    }
}
