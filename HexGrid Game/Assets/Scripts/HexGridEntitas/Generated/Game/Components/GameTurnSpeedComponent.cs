//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public HexGridEntitas.Components.TurnSpeedComponent turnSpeed { get { return (HexGridEntitas.Components.TurnSpeedComponent)GetComponent(GameComponentsLookup.TurnSpeed); } }
    public bool hasTurnSpeed { get { return HasComponent(GameComponentsLookup.TurnSpeed); } }

    public void AddTurnSpeed(float newValue) {
        var index = GameComponentsLookup.TurnSpeed;
        var component = CreateComponent<HexGridEntitas.Components.TurnSpeedComponent>(index);
        component.Value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceTurnSpeed(float newValue) {
        var index = GameComponentsLookup.TurnSpeed;
        var component = CreateComponent<HexGridEntitas.Components.TurnSpeedComponent>(index);
        component.Value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveTurnSpeed() {
        RemoveComponent(GameComponentsLookup.TurnSpeed);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherTurnSpeed;

    public static Entitas.IMatcher<GameEntity> TurnSpeed {
        get {
            if (_matcherTurnSpeed == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.TurnSpeed);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherTurnSpeed = matcher;
            }

            return _matcherTurnSpeed;
        }
    }
}
