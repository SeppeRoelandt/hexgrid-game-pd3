//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    static readonly HexGridEntitas.Components.FindPathComponent findPathComponent = new HexGridEntitas.Components.FindPathComponent();

    public bool goFindPath {
        get { return HasComponent(GameComponentsLookup.FindPath); }
        set {
            if (value != goFindPath) {
                if (value) {
                    AddComponent(GameComponentsLookup.FindPath, findPathComponent);
                } else {
                    RemoveComponent(GameComponentsLookup.FindPath);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherFindPath;

    public static Entitas.IMatcher<GameEntity> FindPath {
        get {
            if (_matcherFindPath == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.FindPath);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherFindPath = matcher;
            }

            return _matcherFindPath;
        }
    }
}
