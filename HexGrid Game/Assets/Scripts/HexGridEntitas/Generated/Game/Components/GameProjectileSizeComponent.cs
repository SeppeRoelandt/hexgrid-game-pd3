//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public HexGridEntitas.Components.ProjectileSizeComponent projectileSize { get { return (HexGridEntitas.Components.ProjectileSizeComponent)GetComponent(GameComponentsLookup.ProjectileSize); } }
    public bool hasProjectileSize { get { return HasComponent(GameComponentsLookup.ProjectileSize); } }

    public void AddProjectileSize(UnityEngine.Vector3 newValue) {
        var index = GameComponentsLookup.ProjectileSize;
        var component = CreateComponent<HexGridEntitas.Components.ProjectileSizeComponent>(index);
        component.Value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceProjectileSize(UnityEngine.Vector3 newValue) {
        var index = GameComponentsLookup.ProjectileSize;
        var component = CreateComponent<HexGridEntitas.Components.ProjectileSizeComponent>(index);
        component.Value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveProjectileSize() {
        RemoveComponent(GameComponentsLookup.ProjectileSize);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherProjectileSize;

    public static Entitas.IMatcher<GameEntity> ProjectileSize {
        get {
            if (_matcherProjectileSize == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.ProjectileSize);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherProjectileSize = matcher;
            }

            return _matcherProjectileSize;
        }
    }
}
