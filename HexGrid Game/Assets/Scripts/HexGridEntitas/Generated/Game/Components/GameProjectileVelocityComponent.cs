//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public HexGridEntitas.Components.ProjectileVelocityComponent projectileVelocity { get { return (HexGridEntitas.Components.ProjectileVelocityComponent)GetComponent(GameComponentsLookup.ProjectileVelocity); } }
    public bool hasProjectileVelocity { get { return HasComponent(GameComponentsLookup.ProjectileVelocity); } }

    public void AddProjectileVelocity(int newValue) {
        var index = GameComponentsLookup.ProjectileVelocity;
        var component = CreateComponent<HexGridEntitas.Components.ProjectileVelocityComponent>(index);
        component.Value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceProjectileVelocity(int newValue) {
        var index = GameComponentsLookup.ProjectileVelocity;
        var component = CreateComponent<HexGridEntitas.Components.ProjectileVelocityComponent>(index);
        component.Value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveProjectileVelocity() {
        RemoveComponent(GameComponentsLookup.ProjectileVelocity);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherProjectileVelocity;

    public static Entitas.IMatcher<GameEntity> ProjectileVelocity {
        get {
            if (_matcherProjectileVelocity == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.ProjectileVelocity);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherProjectileVelocity = matcher;
            }

            return _matcherProjectileVelocity;
        }
    }
}
