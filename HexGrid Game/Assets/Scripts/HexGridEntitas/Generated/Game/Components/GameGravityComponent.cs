//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public HexGridEntitas.Components.GravityComponent gravity { get { return (HexGridEntitas.Components.GravityComponent)GetComponent(GameComponentsLookup.Gravity); } }
    public bool hasGravity { get { return HasComponent(GameComponentsLookup.Gravity); } }

    public void AddGravity(float newValue) {
        var index = GameComponentsLookup.Gravity;
        var component = CreateComponent<HexGridEntitas.Components.GravityComponent>(index);
        component.Value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceGravity(float newValue) {
        var index = GameComponentsLookup.Gravity;
        var component = CreateComponent<HexGridEntitas.Components.GravityComponent>(index);
        component.Value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveGravity() {
        RemoveComponent(GameComponentsLookup.Gravity);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherGravity;

    public static Entitas.IMatcher<GameEntity> Gravity {
        get {
            if (_matcherGravity == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.Gravity);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherGravity = matcher;
            }

            return _matcherGravity;
        }
    }
}
