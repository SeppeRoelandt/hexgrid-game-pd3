﻿using HexGridEntitas.Systems;
using UnityEngine;

namespace HexGridEntitas
{
    public class Entitas : MonoBehaviour
    {
        private RootSystem _systems;

        public Transform SpawnPoint;

        private void Start()
        {
            Contexts  contexts = Contexts.sharedInstance;
            GameContext context = contexts.game;
            context.isInputEnabled = true;

            _systems = new RootSystem(contexts);

            _systems.Initialize();

            context.CreatePlayer(Color.blue, transform.position + Vector3.up * 2, transform.rotation);
            //context.CreateEnemy(Color.red, SpawnPoint.position, SpawnPoint.rotation);
        }

        private void Update()
        {
            _systems.Execute();
            _systems.Cleanup();
        }

        private void OnDestroy()
        {
            _systems.TearDown();
        }
    }
}
