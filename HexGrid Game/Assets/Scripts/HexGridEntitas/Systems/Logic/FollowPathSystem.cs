﻿using System.Collections.Generic;
using Entitas;
using MazeGeneration;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class FollowPathSystem : ReactiveSystem<GameEntity>
    {
        private GameContext _context;

        public FollowPathSystem(Contexts contexts) : base(contexts.game)
        {
            _context = contexts.game;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.FollowPath.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isEnemy && entity.hasPathToTarget;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                List<GameEntity> path = entity.pathToTarget.Tiles;
                if (path != null)
                {
                    path.Reverse();
                    Queue<GameEntity> queue = new Queue<GameEntity>(path);
                    if (queue.Count > entity.distanceFromTarget.Value)
                    {
                        if (entity.tilePosition.X == queue.Peek().tilePosition.X &&
                            entity.tilePosition.Y == queue.Peek().tilePosition.Y &&
                            entity.tilePosition.Z == queue.Peek().tilePosition.Z)
                        {
                            queue.Dequeue();
                        }
                        Vector3 direction = queue.Peek().view.Value.transform.position -
                                            entity.view.Value.transform.position;
                        direction = direction.normalized;
                        if (entity.hasMove)
                        {
                            entity.ReplaceMove(new Vector2(direction.x, direction.z), Time.deltaTime);
                        }
                        else
                        {
                            entity.AddMove(new Vector2(direction.x, direction.z), Time.deltaTime);
                        }
                    }
                }
                entity.goFollowPath = false;
            }
        }
    }
}