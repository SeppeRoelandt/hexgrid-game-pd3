﻿using System.Collections.Generic;
using Entitas;
using MazeGeneration;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class FindPathSystem : ReactiveSystem<GameEntity>
    {
        private GameContext _context;

        public FindPathSystem(Contexts contexts) : base(contexts.game)
        {
            _context = contexts.game;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.FindPath.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isEnemy && entity.hasRoomId && entity.hasTarget &&
                   entity.roomId.Value == _context.GetEntityWithName("Player").roomId.Value;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                if (entity.roomId.Value == _context.GetEntityWithName("Player").roomId.Value)
                {
                    GameEntity roomEntity = _context.GetEntityWithName(entity.roomId.Value.ToString());
                    GameEntity[] room = roomEntity.roomTiles.Tiles;
                    int size = roomEntity.roomSize.Value;

                    if (entity.hasPathToTarget)
                    {
                        entity.ReplacePathToTarget(FindPath(
                            room[(entity.tilePosition.X * (size * 2 + 1) + entity.tilePosition.Z)],
                            room[(_context.GetEntityWithName("Player").tilePosition.X * (size * 2 + 1) +
                                  _context.GetEntityWithName("Player").tilePosition.Z)], size, room));
                    }
                    else
                    {
                        entity.AddPathToTarget(FindPath(
                            room[(entity.tilePosition.X * (size * 2 + 1) + entity.tilePosition.Z)],
                            room[(_context.GetEntityWithName("Player").tilePosition.X * (size * 2 + 1) +
                                  _context.GetEntityWithName("Player").tilePosition.Z)], size, room));
                    }
                }
                entity.goFindPath = false;
            }
        }

        public List<GameEntity> FindPath(GameEntity startcell, GameEntity endcell, int size, GameEntity[] room)
        {
            if (startcell.tileState.State != TileState.Walkable || endcell.tileState.State != TileState.Walkable)
            {
                return null;
            }
            Dictionary<GameEntity, int> pathValues = new Dictionary<GameEntity, int>();

            PriorityQueue<GameEntity> queue = new PriorityQueue<GameEntity>((left, right) =>
            {
                //manhattan
                var distance1 =
                    Mathf.Max(Mathf.Abs(left.tilePosition.X - endcell.tilePosition.X) +
                              Mathf.Abs(left.tilePosition.Y - endcell.tilePosition.Y) +
                              Mathf.Abs(left.tilePosition.Z - endcell.tilePosition.Z)) / 2 + left.penalty.Value;
                var distance2 =
                    Mathf.Max(Mathf.Abs(right.tilePosition.X - endcell.tilePosition.X) +
                              Mathf.Abs(right.tilePosition.Y - endcell.tilePosition.Y) +
                              Mathf.Abs(right.tilePosition.Z - endcell.tilePosition.Z)) / 2 +
                    right.penalty.Value;

                return distance1.CompareTo(distance2);
            });

            Dictionary<GameEntity, GameEntity> cameFrom = new Dictionary<GameEntity, GameEntity>();


            bool[] visited = new bool[room.Length];

            //Start van de start positie
            GameEntity start = startcell;
            queue.Enqueue(start);
            pathValues[start] = 0;

            bool found = false;
            while (queue.Count > 0 && !found)
            {
                queue.IsConsistent();
                GameEntity current = queue.Dequeue();

                //early exit
                if (current == endcell)
                {
                    found = true;
                }

                List<GameEntity> unvisitedNeighbours = FilterNeighbours(current, visited, size, room);
                foreach (var neighbour in unvisitedNeighbours)
                {
                    cameFrom[neighbour] = current;

                    pathValues[neighbour] = pathValues[current] + neighbour.penalty.Value;
                    queue.Enqueue(neighbour);
                }

                visited[current.tilePosition.X * (size * 2 + 1) + current.tilePosition.Z] = true;
            }
            if (found)
            {
                GameEntity pathPart = endcell;
                List<GameEntity> path = new List<GameEntity>();
                while (pathPart != startcell)
                {
                    pathPart = cameFrom[pathPart];
                    path.Add(pathPart);
                }
                return path;
            }
            
            return null;
        }

        private List<GameEntity> FilterNeighbours(GameEntity current, bool[] visited, int size, GameEntity[] room)
        {
            // collect unvisited neighbours
            List<GameEntity> unvisitedNeighbours = new List<GameEntity>(6);

            if (current.tilePosition.X < (size * 2) &&
                !visited[(current.tilePosition.X + 1) * (size * 2 + 1) + current.tilePosition.Z] &&
                room[(current.tilePosition.X + 1) * (size * 2 + 1) + current.tilePosition.Z].tileState.State ==
                TileState.Walkable &&
                room[(current.tilePosition.X + 1) * (size * 2 + 1) + current.tilePosition.Z].enabled.Value)
                unvisitedNeighbours.Add(room[(current.tilePosition.X + 1) * (size * 2 + 1) + current.tilePosition.Z]);

            if (current.tilePosition.X > 0 &&
                !visited[(current.tilePosition.X - 1) * (size * 2 + 1) + current.tilePosition.Z] &&
                room[(current.tilePosition.X - 1) * (size * 2 + 1) + current.tilePosition.Z].tileState.State ==
                TileState.Walkable &&
                room[(current.tilePosition.X - 1) * (size * 2 + 1) + current.tilePosition.Z].enabled.Value)
                unvisitedNeighbours.Add(room[(current.tilePosition.X - 1) * (size * 2 + 1) + current.tilePosition.Z]);

            if (current.tilePosition.Z < (size * 2) &&
                !visited[current.tilePosition.X * (size * 2 + 1) + (current.tilePosition.Z + 1)] &&
                room[current.tilePosition.X * (size * 2 + 1) + (current.tilePosition.Z + 1)].tileState.State ==
                TileState.Walkable &&
                room[current.tilePosition.X * (size * 2 + 1) + (current.tilePosition.Z + 1)].enabled.Value)
                unvisitedNeighbours.Add(room[current.tilePosition.X * (size * 2 + 1) + (current.tilePosition.Z + 1)]);

            if (current.tilePosition.Z > 0 &&
                !visited[current.tilePosition.X * (size * 2 + 1) + (current.tilePosition.Z - 1)] &&
                room[current.tilePosition.X * (size * 2 + 1) + (current.tilePosition.Z - 1)].tileState.State ==
                TileState.Walkable &&
                room[current.tilePosition.X * (size * 2 + 1) + (current.tilePosition.Z - 1)].enabled.Value)
                unvisitedNeighbours.Add(room[current.tilePosition.X * (size * 2 + 1) + (current.tilePosition.Z - 1)]);

            if (current.tilePosition.X < (size * 2) && current.tilePosition.Z > 0 &&
                !visited[(current.tilePosition.X + 1) * (size * 2 + 1) + (current.tilePosition.Z - 1)] &&
                room[(current.tilePosition.X + 1) * (size * 2 + 1) + (current.tilePosition.Z - 1)].tileState.State ==
                TileState.Walkable &&
                room[(current.tilePosition.X + 1) * (size * 2 + 1) + (current.tilePosition.Z - 1)].enabled.Value)
                unvisitedNeighbours.Add(
                    room[(current.tilePosition.X + 1) * (size * 2 + 1) + (current.tilePosition.Z - 1)]);

            if (current.tilePosition.X > 0 && current.tilePosition.Z < (size * 2) &&
                !visited[(current.tilePosition.X - 1) * (size * 2 + 1) + (current.tilePosition.Z + 1)] &&
                room[(current.tilePosition.X - 1) * (size * 2 + 1) + (current.tilePosition.Z + 1)].tileState.State ==
                TileState.Walkable &&
                room[(current.tilePosition.X - 1) * (size * 2 + 1) + (current.tilePosition.Z + 1)].enabled.Value)
                unvisitedNeighbours.Add(
                    room[(current.tilePosition.X - 1) * (size * 2 + 1) + (current.tilePosition.Z + 1)]);


            return unvisitedNeighbours;
        }
    }
}