﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;
using Entitas.Unity;
using PickUp;

namespace HexGridEntitas.Systems.Logic
{
    public class PickUpSystem: ReactiveSystem<GameEntity>
    {
    

        public PickUpSystem(Contexts contexts) : base(contexts.game)
        {
          
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Collision.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isPickUp && entity.collision.Other.tag == "Player" && entity.hasView && entity.hasPickUpType;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                //Set players roomid to the rooms roomid
                var entityLink = entity.collision.Other.gameObject.GetEntityLink();
                if (entityLink != null)
                {
                    //Get other entity
                    GameEntity otherEntity = (GameEntity) entityLink.entity;

                    switch (entity.pickUpType.Type)
                    {
                        case PickUpType.ProjectileSpeedIncrease:
                            otherEntity.fireRate.Value -= 0.05f;
                            entity.isDestroyed = true;
                            break;
                        case PickUpType.ProjectileSizeIncrease:
                            otherEntity.projectileSize.Value += Vector3.one * 0.1f;
                            entity.isDestroyed = true;
                            break;
                        case PickUpType.ProjectileDamageIncrease:
                            otherEntity.projectileDamage.Value += 3;
                            entity.isDestroyed = true;
                            break;
                        case PickUpType.MovementSpeedIncrease:
                            otherEntity.moveSpeed.Value += 0.5f;
                            entity.isDestroyed = true;
                            break;
                        case PickUpType.Heal:
                            if (otherEntity.health.Value < otherEntity.health.MaxValue)
                            {
                                otherEntity.ReplaceHealth(otherEntity.health.Value + 20f, otherEntity.health.MaxValue);
                                if (otherEntity.health.Value > otherEntity.health.MaxValue)
                                {
                                    otherEntity.health.Value = otherEntity.health.MaxValue;
                                }
                                entity.isDestroyed = true;
                            }
                            break;
                    }
                }
            }
        }
    }
}