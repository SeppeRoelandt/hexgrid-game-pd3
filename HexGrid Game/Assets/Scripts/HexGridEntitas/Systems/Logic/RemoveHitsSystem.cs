﻿using System;
using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class RemoveHitsSystem : ReactiveSystem<GameEntity>
    {

        public RemoveHitsSystem(Contexts contexts) : base(contexts.game)
        {
           
        }


        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Hit.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasHit;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                if (entity.hasHit)
                    entity.RemoveHit();
                if (entity.hasCollision)
                    entity.RemoveCollision();
            }
        }
    }
}
