﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class FireSystem : ReactiveSystem<GameEntity>
    {
        private GameContext _context;

        public FireSystem(Contexts contexts) : base(contexts.game)
        {
            _context = contexts.game;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Fired);
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasView && entity.hasFireRate ;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                GameObject gameObject = entity.view.Value;
                Transform firePosition = gameObject.GetComponent<Fireable>().FirePosition;
                Vector3 size = entity.projectileSize.Value;
                float delay = entity.fireRate.Value;
                float timeOfLastShot = entity.fireRate.TimeOfLastShot;

                //can this entity shoot again
                if (timeOfLastShot <= Time.time)
                {
                    //set time when this entity can shoot again
                    entity.fireRate.TimeOfLastShot = Time.time + delay;
                    //shoot projectile
                    _context.CreateProjectile(firePosition.position, firePosition.rotation, size, firePosition.forward * entity.projectileVelocity.Value, entity.projectileDamage.Value);
                }
            }
        }
    }
}
