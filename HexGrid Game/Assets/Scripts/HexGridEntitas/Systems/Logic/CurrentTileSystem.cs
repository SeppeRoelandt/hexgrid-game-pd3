﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;
using Entitas.Unity;

namespace HexGridEntitas.Systems.Logic
{
    public class CurrentTileSystem: ReactiveSystem<GameEntity>
    {
    

        public CurrentTileSystem(Contexts contexts) : base(contexts.game)
        {
          
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Collision.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isTile && (entity.collision.Other.tag == "Player" || entity.collision.Other.tag == "Enemy") && entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                //Set room pos
                var entityLink = entity.collision.Other.gameObject.GetEntityLink();
                if (entityLink != null)
                {
                    //Get other entity
                    GameEntity otherEntity = (GameEntity) entityLink.entity;
                    if (!otherEntity.hasTilePosition)
                    {
                        otherEntity.AddTilePosition(entity.tilePosition.X, entity.tilePosition.Y, entity.tilePosition.Z);
                    }
                    if (otherEntity.hasTilePosition)
                    {
                        otherEntity.ReplaceTilePosition(entity.tilePosition.X, entity.tilePosition.Y, entity.tilePosition.Z);
                    }
                }
            }
        }
    }
}