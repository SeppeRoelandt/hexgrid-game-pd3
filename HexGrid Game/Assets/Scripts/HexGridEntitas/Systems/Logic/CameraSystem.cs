﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;
using Entitas.Unity;

namespace HexGridEntitas.Systems.Logic
{
    public class CameraSystem: ReactiveSystem<GameEntity>
    {
    

        public CameraSystem(Contexts contexts) : base(contexts.game)
        {
          
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Collision.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isRoom && entity.collision.Other.tag == "Player" && entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                //move camera to room position
                Camera.main.transform.position = new Vector3(entity.view.Value.transform.position.x,
                    Camera.main.transform.position.y, entity.view.Value.transform.position.z);

                //Set players roomid to the rooms roomid
                var entityLink = entity.collision.Other.gameObject.GetEntityLink();
                if (entityLink != null)
                {
                    //Get other entity
                    GameEntity otherEntity = (GameEntity) entityLink.entity;
                    if (!otherEntity.hasRoomId)
                    {
                        otherEntity.AddRoomId(entity.roomId.Value);
                    }
                    if (otherEntity.hasRoomId)
                    {
                        otherEntity.ReplaceRoomId(entity.roomId.Value);
                    }
                }
            }
        }
    }
}