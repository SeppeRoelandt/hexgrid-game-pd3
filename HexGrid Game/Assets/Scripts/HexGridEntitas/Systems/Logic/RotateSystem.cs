﻿using System;
using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class RotateSystem : ReactiveSystem<GameEntity>
    {
        public RotateSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Turn.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasTurn && entity.hasTurnSpeed && entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                GameObject gameObject = entity.view.Value;
                float speed = entity.turnSpeed.Value;
                Vector3 amount = entity.turn.Amount;
                float deltaTime = entity.turn.DeltaTime;

                Quaternion previousRot = gameObject.transform.rotation;

                if (entity.isPlayer)
                {
                    Vector3 mouseWorldSpace = Camera.main.ScreenToWorldPoint(amount);
                    gameObject.transform.LookAt(mouseWorldSpace, Vector3.up);
                }
                else if (entity.isEnemy)
                {
                    gameObject.transform.LookAt(new Vector3(amount.x,0,amount.y), Vector3.up);
                }
             
                gameObject.transform.eulerAngles = new Vector3(0, gameObject.transform.eulerAngles.y, 0);
                gameObject.transform.rotation = Quaternion.Slerp(previousRot, gameObject.transform.rotation, deltaTime * speed);
            }
        }
    }
}