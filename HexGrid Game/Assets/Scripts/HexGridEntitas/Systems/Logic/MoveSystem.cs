﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class MoveSystem : ReactiveSystem<GameEntity>
    {
        public MoveSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Move.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasMoveSpeed && entity.hasMove && entity.hasGravity &&entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                GameObject gameObject = entity.view.Value;
                float speed = entity.moveSpeed.Value;
                float gravity = entity.gravity.Value;
                Vector2 amount = entity.move.Amount;
                float deltaTime = entity.move.DeltaTime;

                CharacterController characterController = gameObject.GetComponent<CharacterController>();

                Vector3 moveDirection = new Vector3(amount.x, 0, amount.y).normalized * speed * deltaTime;
                moveDirection = moveDirection + Vector3.down * gravity;
                characterController.Move(moveDirection);
            }
        }
    }
}