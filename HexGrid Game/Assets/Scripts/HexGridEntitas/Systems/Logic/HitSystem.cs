﻿using System;
using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class HitSystem : ReactiveSystem<GameEntity>
    {

        public HitSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Collision.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasView && entity.hasProjectileDamage;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                if (entity.hasCollision)
                {
                    var entityLink = entity.collision.Other.gameObject.GetEntityLink();
                    if (entityLink != null)
                    {
                        //Get the damage of this projectile
                        float damage = entity.projectileDamage.Value;
                        //Get other entity
                        GameEntity otherEntity = (GameEntity) entityLink.entity;

                        //Apply damage to entity
                        if (!otherEntity.hasHit)
                        {
                            otherEntity.AddHit(damage);
                        }
                    }
                    //Destroy projectile
                    if (entity.isProjectile)
                    {
                        entity.isDestroyed = true;
                    }
                }
            }
        }

    }
}
