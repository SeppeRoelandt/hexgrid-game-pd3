﻿namespace HexGridEntitas.Systems.Logic
{
    public class LogicSystems : Feature
    {
        public LogicSystems(Contexts contexts) : base("Logic Systems")
        {
            Add(new FireSystem(contexts));
            Add(new HitSystem(contexts));
            Add(new HealthSystem(contexts));
            Add(new MoveSystem(contexts));
            Add(new RotateSystem(contexts));
            Add(new LifetimeSystem(contexts));
            Add(new EntitySpawnSystem(contexts));
            Add(new PickUpSystem(contexts));
            Add(new FindPathSystem(contexts));
            Add(new FollowPathSystem(contexts));
            Add(new CurrentTileSystem(contexts));
            Add(new CameraSystem(contexts));
            Add(new RemoveHitsSystem(contexts));
        }
    }
}