﻿using System;
using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using MazeGeneration;
using PickUp;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class EntitySpawnSystem : ReactiveSystem<GameEntity>
    {
        private GameContext _context;

        public EntitySpawnSystem(Contexts contexts) : base(contexts.game)
        {
            _context = contexts.game;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Collision.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasView && entity.isRoom && entity.hasPseudoRandom &&
                   entity.collision.Other.tag == "Player" && !entity.hasEntitiesInRoom;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                var entityLink = entity.collision.Other.gameObject.GetEntityLink();
                if (entityLink != null)
                {
                    //Get Cells
                    GameEntity[] room = entity.roomTiles.Tiles;
                    //Get Walkable cells
                    List<GameEntity> walkableCells = new List<GameEntity>();
                    foreach (var tile in room)
                    {
                        if (tile.tileState.State == TileState.Walkable/* && tile.hasView*/)
                        {
                            walkableCells.Add(tile);
                        }
                    }

                    //Get PseudoRandom
                    System.Random pseudoRandom = entity.pseudoRandom.PseudoRandom;

                    //Random amount of enemies
                    int amountOfEnemies = pseudoRandom.Next(0, 4);

                    //Start Room has no enemies
                    if (entity.tilePosition.X == entity.tilePosition.Y &&
                        entity.tilePosition.Y == entity.tilePosition.Z)
                    {
                        amountOfEnemies = 0;
                    }

                    List<GameEntity> spawnedEntities = new List<GameEntity>();

                    for (int i = 0; i < amountOfEnemies; i++)
                    {
                        int randomTile = pseudoRandom.Next(0, walkableCells.Count);
                        Transform spawnPoint = walkableCells[randomTile].view.Value.transform;
                        //random enemy
                        int enemyType = pseudoRandom.Next(0, 4);

                        GameEntity enemy = null;
                        //spawn enemies
                        if (enemyType == 0)
                        {
                            //Spawn turret
                             enemy = _context.CreateTurret(Color.red, spawnPoint.position + Vector3.up * 2,
                                spawnPoint.rotation, entity.roomId.Value);
                        }
                        else
                        {
                            //Spawn enemies
                            enemy = _context.CreateEnemy(Color.red, spawnPoint.position + Vector3.up * 2,
                                spawnPoint.rotation, entity.roomId.Value);
                        }
                        if (enemy != null)
                        {
                            spawnedEntities.Add(enemy);
                        }
                    }

                    //Random amount of PowerUp
                    int amountOfPowerUps = pseudoRandom.Next(0, 4);

                    for (int i = 0; i < amountOfPowerUps; i++)
                    {
                        int randomTile = pseudoRandom.Next(0, walkableCells.Count);
                        if (walkableCells[randomTile].hasView)
                        {
                            //spawnpoint
                            Transform spawnPoint = walkableCells[randomTile].view.Value.transform;

                            GameEntity powerUp = _context.CreatePowerUp(Color.cyan, spawnPoint.position + Vector3.up * 2,
                                spawnPoint.rotation);

                            int randomPickUpType = pseudoRandom.Next(0, 5);
                            switch (randomPickUpType)
                            {
                                case 0:
                                    powerUp.AddPickUpType(PickUpType.ProjectileSpeedIncrease);
                                    powerUp.ReplaceColor(Color.blue);
                                    break;
                                case 1:
                                    powerUp.AddPickUpType(PickUpType.ProjectileSizeIncrease);
                                    powerUp.ReplaceColor(new Color(0,0,0.8f,1));
                                    break;
                                case 2:
                                    powerUp.AddPickUpType(PickUpType.ProjectileDamageIncrease);
                                    powerUp.ReplaceColor(new Color(0, 0, 0.6f,1));
                                    break;
                                case 3:
                                    powerUp.AddPickUpType(PickUpType.MovementSpeedIncrease);
                                    powerUp.ReplaceColor(new Color(1, 0.64f, 0, 1));
                                    break;
                                case 4:
                                    powerUp.AddPickUpType(PickUpType.Heal);
                                    powerUp.ReplaceColor(Color.green);
                                    break;
                            }

                            spawnedEntities.Add(powerUp);
                        }
                    }
                    entity.AddEntitiesInRoom(spawnedEntities);
                }
            }
        }
    }
}
