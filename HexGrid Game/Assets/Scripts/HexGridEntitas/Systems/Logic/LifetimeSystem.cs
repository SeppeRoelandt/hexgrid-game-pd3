﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class LifetimeSystem : ICleanupSystem
    {
        private readonly IGroup<GameEntity> _dyingEntities;

        public LifetimeSystem(Contexts contexts)
        {
            _dyingEntities = contexts.game.GetGroup(GameMatcher.Lifetime);
        }

        public void Cleanup()
        {
            foreach (var entity in _dyingEntities)
            {
                if (entity.hasLifetime && entity.lifetime.Value < Time.time)
                {
                    entity.isDestroyed = true;
                }
            }
        }
    }
}
