﻿using System.Collections.Generic;
using Entitas;

namespace HexGridEntitas.Systems.Logic
{
    public class HealthSystem: ReactiveSystem<GameEntity>
    {
    

        public HealthSystem(Contexts contexts) : base(contexts.game)
        {
          
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Hit.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasHit && entity.hasHealth;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.ReplaceHealth(entity.health.Value - entity.hit.Value, entity.health.MaxValue);
                if (entity.health.Value <= 0)
                {
                    entity.isDestroyed = true;
                }
            }
        }
    }
}