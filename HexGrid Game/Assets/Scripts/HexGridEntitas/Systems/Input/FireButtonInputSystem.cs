﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace HexGridEntitas.Systems.Input
{
    public class FireButtonInputSystem : IExecuteSystem, ICleanupSystem
    {
        private IGroup<GameEntity> _identifiablePlayer;
        private GameContext _context;

        public FireButtonInputSystem(Contexts contexts)
        {
            _identifiablePlayer = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Player, GameMatcher.FireRate));
            _context = contexts.game;
        }

        public void Execute()
        {
            if (!_context.isInputEnabled)
                return;

            foreach (var entity in _identifiablePlayer)
            {
                if (UnityEngine.Input.GetButton("Fire1"))
                {
                    entity.hasFired = true;
                }
            }
        }

        public void Cleanup()
        {
            foreach (var entity in _identifiablePlayer)
            {
                entity.hasFired = false;
            }
        }
    }
}
