﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Systems.Input
{
    public class MoveAxisInputSystem : IExecuteSystem, ICleanupSystem
    {
        private IGroup<GameEntity> _identifiablePlayer;
        private GameContext _context;

        public MoveAxisInputSystem(Contexts contexts)
        {
            _identifiablePlayer = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Player));
            _context = contexts.game;
        }

        public void Execute()
        {
            if (!_context.isInputEnabled)
                return;

            float deltaTime = Time.deltaTime;

            foreach (var entity in _identifiablePlayer)
            {
                float vertical = UnityEngine.Input.GetAxis("Vertical");
                float horizontal = UnityEngine.Input.GetAxis("Horizontal");

                Vector2 moveDirection = new Vector2(horizontal, vertical);

                if ((!Mathf.Approximately(0.0f, horizontal) || !Mathf.Approximately(0.0f, vertical)) && !entity.hasMove)
                {
                    entity.AddMove(moveDirection, deltaTime);
                }
            }
        }

        public void Cleanup()
        {
            foreach (var entity in _identifiablePlayer)
            {
                if(entity.hasMove)
                    entity.RemoveMove();
            }
        }
    }
}
