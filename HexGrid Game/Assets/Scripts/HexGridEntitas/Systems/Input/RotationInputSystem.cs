﻿using Entitas;
using UnityEngine;

namespace HexGridEntitas.Systems.Input
{
    public class RotationInputSystem : IExecuteSystem, ICleanupSystem
    {
        private IGroup<GameEntity> _identifiablePlayer;
        private GameContext _context;

        public RotationInputSystem(Contexts contexts)
        {
            _identifiablePlayer = contexts.game.GetGroup(GameMatcher.AllOf(GameMatcher.Player));
            _context = contexts.game;
        }

        public void Execute()
        {
            if (!_context.isInputEnabled)
                return;

            float deltaTime = Time.deltaTime;

            foreach (var entity in _identifiablePlayer)
            {
                if (entity.hasTurn)
                    return;
                
                Vector3 moveDirection = UnityEngine.Input.mousePosition;
                entity.AddTurn(moveDirection, deltaTime);
            }
        }

        public void Cleanup()
        {
            foreach (var entity in _identifiablePlayer)
            {
                if(entity.hasTurn)
                    entity.RemoveTurn();
            }
        }
    }
}
