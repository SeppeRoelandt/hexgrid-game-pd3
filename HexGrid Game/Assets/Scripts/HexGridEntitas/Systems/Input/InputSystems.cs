﻿namespace HexGridEntitas.Systems.Input
{
    public class InputSystems : Feature
    {
        public InputSystems(Contexts contexts) : base("Input Systems")
        {
            Add(new FireButtonInputSystem(contexts));
            Add(new MoveAxisInputSystem(contexts));
            Add(new RotationInputSystem(contexts));
        }
    }
}