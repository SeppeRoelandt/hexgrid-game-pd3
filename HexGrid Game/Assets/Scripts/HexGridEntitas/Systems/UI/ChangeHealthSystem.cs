﻿using System.Collections.Generic;
using Entitas;
using HexGridEntitas.Views;
using UnityEngine;

namespace HexGridEntitas.Systems.UI
{
    public class ChangeHealthSystem : ReactiveSystem<GameEntity>
    {
        public ChangeHealthSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Health.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                GameObject gameObject =  entity.view.Value;
                gameObject.GetComponent<HealthVisualizer>().Health = entity.health.Value / entity.health.MaxValue;
            }
        }
    }
}