﻿using System;
using System.Collections.Generic;
using Entitas;
using HexGridEntitas.Views;
using UnityEngine;

namespace HexGridEntitas.Systems.UI
{
    public class ChangeColorSystem : ReactiveSystem<GameEntity>
    {
        public ChangeColorSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Color.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasColor && entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                GameObject gameObject = entity.view.Value;
                Color color = entity.color.Value;
                gameObject.GetComponent<Colorized>().Color = color;

            }
        }
    }
}
