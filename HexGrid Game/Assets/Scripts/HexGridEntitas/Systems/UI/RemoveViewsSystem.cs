﻿using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

namespace HexGridEntitas.Systems.UI
{
    public class RemoveViewsSystem : ReactiveSystem<GameEntity>
    {
        public RemoveViewsSystem(Contexts contexts) : base(contexts.game)
        {
            
        }

        public void Cleanup()
        {

        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Prefab.Removed(), GameMatcher.Destroyed.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasView;
        }

        protected override void Execute(List<GameEntity> entities)
        {

            foreach (var entity in entities)
            {
                GameObject gameObject = entity.view.Value;
                gameObject.Unlink();
                entity.RemoveView();
                GameObject.Destroy(gameObject);
                
            }
        }
    }
}
