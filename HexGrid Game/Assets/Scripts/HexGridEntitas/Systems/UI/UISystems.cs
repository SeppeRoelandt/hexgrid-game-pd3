﻿using HexGridEntitas.Systems.Logic;

namespace HexGridEntitas.Systems.UI
{
    public class UISystems : Feature
    {
        public UISystems(Contexts contexts): base("UI Systems")
        {
            Add(new ViewsSystem(contexts));
            Add(new ChangeColorSystem(contexts));
            Add(new ChangeHealthSystem(contexts));
            Add(new RemoveViewsSystem(contexts));
            Add(new TileStateSystem(contexts));
        }
    }
}
