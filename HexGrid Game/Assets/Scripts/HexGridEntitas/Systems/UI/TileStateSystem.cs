﻿using System.Collections.Generic;
using Entitas;
using MazeGeneration;
using UnityEngine;

namespace HexGridEntitas.Systems.Logic
{
    public class TileStateSystem : ReactiveSystem<GameEntity>
    {
        public TileStateSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.Tile.Added());
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.hasTileState;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                switch (entity.tileState.State)
                {
                    case TileState.Walkable:
                        entity.AddPrefab("Tile Walkable");
                        break;
                    case TileState.Wall:
                        entity.AddPrefab("Tile Wall");
                        break;
                    case TileState.Solid:
                        entity.AddPrefab("Tile Wall");
                        break;
                    case TileState.Path:
                        entity.AddPrefab("Tile Walkable");
                        break;

                }
            }
        }
    }
}