﻿using HexGridEntitas.Systems.Input;
using HexGridEntitas.Systems.Logic;
using HexGridEntitas.Systems.UI;

namespace HexGridEntitas.Systems
{
    public class RootSystem : Feature
    {
        public RootSystem(Contexts contexts) : base( "Root System")
        {
            Add(new InputSystems(contexts));
            Add(new LogicSystems(contexts));
            Add(new UISystems(contexts));
            Add(new DestroySystem(contexts));
        }
    }
}
