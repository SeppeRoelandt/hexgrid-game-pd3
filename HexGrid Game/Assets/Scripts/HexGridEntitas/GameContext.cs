﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MazeGeneration;
using UnityEngine;
using Random = UnityEngine.Random;

public sealed partial class GameContext
{
    public GameEntity CreatePlayer(Color color, Vector3 startPosition, Quaternion startRotation)
    {
        var entity = CreateEntity();
        entity.isPlayer = true;
        entity.AddStartPosition(startPosition);
        entity.AddStartRotation(startRotation);
        entity.AddMoveSpeed(8f);
        entity.AddTurnSpeed(5f);
        entity.AddGravity(9.81f);
        entity.AddPrefab("Player");
        entity.AddHealth(100f, 100f);
        entity.AddFireRate(0.4f,Time.time);
        entity.AddProjectileSize(Vector3.one * 0.3f);
        entity.AddProjectileDamage(8);
        entity.AddProjectileVelocity(10);
        entity.AddColor(color);
        entity.AddName("Player");

        return entity;
    }

    public GameEntity CreateEnemy(Color color, Vector3 startPosition, Quaternion startRotation, int roomId)
    {
        var entity = CreateEntity();
        entity.AddIndex("Enemy");
        entity.isEnemy = true;
        entity.AddStartPosition(startPosition);
        entity.AddStartRotation(startRotation);
        entity.AddMoveSpeed(5f);
        entity.AddTurnSpeed(2f);
        entity.AddGravity(9.81f);
        entity.AddPrefab("Enemy");
        entity.AddHealth(20f, 20f);
        entity.AddFireRate(0.5f, Time.time);
        entity.AddProjectileSize(Vector3.one * 0.5f);
        entity.AddProjectileDamage(10f);
        entity.AddProjectileVelocity(10);
        entity.AddDistanceFromTarget(3);
        entity.AddColor(color);
        entity.AddRoomId(roomId);
        return entity;
    }

    public GameEntity CreateTurret(Color color, Vector3 startPosition, Quaternion startRotation, int roomId)
    {
        var entity = CreateEntity();
        entity.AddIndex("Turret");
        entity.isEnemy = true;
        entity.AddStartPosition(startPosition);
        entity.AddStartRotation(startRotation);
        entity.AddTurnSpeed(5f);
        entity.AddGravity(9.81f);
        entity.AddPrefab("Turret");
        entity.AddHealth(35f, 35f);
        entity.AddFireRate(0.1f, Time.time);
        entity.AddProjectileSize(Vector3.one * 0.2f);
        entity.AddProjectileDamage(2f);
        entity.AddProjectileVelocity(15);
        entity.AddColor(color);
        entity.AddRoomId(roomId);
        return entity;
    }

    public GameEntity CreateProjectile(Vector3 position, Quaternion rotation, Vector3 size, Vector3 firingDirection, float projectileDamage)
    {
        var entity = CreateEntity();
        entity.isProjectile = true;
        entity.AddStartPosition(position);
        entity.AddStartRotation(rotation);
        entity.AddStartSize(size);
        entity.AddStartVelocity(firingDirection);
        entity.AddPrefab("Projectile");
        entity.AddLifetime(Time.time + 2);
        entity.AddProjectileDamage(projectileDamage);

        return entity;
    }

    public GameEntity CreateTile(Vector3 position, int x, int y , int z, int id, int roomId, int penalty, bool enabled)
    {
        var entity = CreateEntity();
        entity.AddRoomId(roomId);
        entity.AddId(id);
        entity.AddStartPosition(position);
        entity.AddTilePosition(x,y,z);
        entity.AddEnabled(enabled);
        entity.AddPenalty(penalty);

        return entity;
    }

    public GameEntity CreateEnd(Vector3 position , bool enabled)
    {
        var entity = CreateEntity();
        entity.AddStartPosition(position);
        entity.AddPrefab("End");
        entity.AddEnabled(enabled);
        return entity;
    }

    public GameEntity CreateRoom(Vector3 position, int x, int y, int z, int roomSize, int roomId, System.Random pseudo, bool enabled)
    {
        var entity = CreateEntity();
        entity.AddName(roomId.ToString());
        entity.isRoom = true;
        entity.AddPrefab("Room");
        entity.AddRoomId(roomId);
        entity.AddRoomSize(roomSize);
        entity.AddStartPosition(position);
        entity.AddTilePosition(x, y, z);
        entity.AddEnabled(enabled);
        entity.AddWallPositions(WallPositions.Bottom | WallPositions.BottomLeft | WallPositions.BottomRight |
                               WallPositions.Top | WallPositions.TopLeft | WallPositions.TopRight);
        entity.AddPseudoRandom(pseudo);

        return entity;
    }

    public GameEntity CreatePowerUp(Color color, Vector3 position, Quaternion rotation)
    {
        var entity = CreateEntity();
        entity.isPickUp = true;
        entity.AddPrefab("PowerUp");
        entity.AddStartPosition(position);
        entity.AddStartRotation(rotation);
        entity.AddColor(color);

        return entity;
    }
}
