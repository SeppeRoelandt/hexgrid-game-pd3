﻿using System;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    private GameObject _lose;

    void Start()
    {
        _lose = GameObject.FindGameObjectWithTag("lose");
        _lose.SetActive(false);
    }

    void OnDestroy()
    {
        if (_lose != null)
        {
            _lose.SetActive(true);
        }
    }
}
