﻿namespace BehaviourTrees
{
   public class Invertor : DecoratorNode
    {
        public Invertor(INode child) : base(child)
        {
        }

        public override BehaviourState Tick()
        {
            BehaviourState state = Child.Tick();
            switch (state)
            {
                case BehaviourState.Success:
                    return BehaviourState.Failure;
                case BehaviourState.Failure:
                    return BehaviourState.Success;
                default:
                    return state;
            }
        }
    }
}
