﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BehaviourTrees
{
    public abstract class DecoratorNode : INode
    {
        protected INode Child;

        protected DecoratorNode(INode child)
        {
            Child = child;
        }

        public abstract BehaviourState Tick();
    }
}
