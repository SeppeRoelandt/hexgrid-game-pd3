﻿namespace BehaviourTrees
{
    public enum BehaviourState
    {
        Success,
        Failure,
        Running
    }
}