﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BehaviourTrees
{
    public abstract class CompositeNode : INode
    {
        protected List<INode> ChildNodes;

        protected CompositeNode(List<INode> childNodes)
        {
            ChildNodes = childNodes;
        }

        public abstract BehaviourState Tick();
    }
}
