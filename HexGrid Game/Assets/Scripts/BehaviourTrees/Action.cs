﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BehaviourTrees
{
    public delegate BehaviourState ActionDelegate();
    public class Action : INode
    {
        private ActionDelegate _actionDelegate;

        public Action(ActionDelegate actionDelegate)
        {
            _actionDelegate = actionDelegate;
        }


        public BehaviourState Tick()
        {
            if (_actionDelegate == null)
                return BehaviourState.Failure;

            return _actionDelegate();
        }
    }
}
