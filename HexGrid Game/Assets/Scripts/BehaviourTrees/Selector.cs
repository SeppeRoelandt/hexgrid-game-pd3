﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BehaviourTrees;

namespace BehaviourTrees
{
    public class Selector : CompositeNode
    {
        public Selector(params INode[] childNodes) : base(new List<INode>(childNodes))
        {

        }

        public override BehaviourState Tick()
        {
            foreach (var childNode in ChildNodes)
            {
                BehaviourState state = childNode.Tick();

                switch (state)
                {
                    case BehaviourState.Success:
                        return state;
                    case BehaviourState.Running:
                        return state;
                }
            }
            return BehaviourState.Failure;
        }
    }
}
