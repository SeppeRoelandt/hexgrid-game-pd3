﻿using System;
using BehaviourTrees;
using UnityEngine;
using Entitas.Unity;

class TurretAIBehaviour :MonoBehaviour
{
    private Contexts _contexts;
    private GameContext _context;
    private GameEntity _entity;
    private INode _root;

    private void Start()
    {
        _contexts = Contexts.sharedInstance;
        _context = _contexts.game;
        _entity = (GameEntity) gameObject.GetEntityLink().entity;

        _root = new Selector(
            new Sequence( 
                new Condition(HasNoTargetSelected),
                new BehaviourTrees.Action(SelectTarget)
                ),
            new Sequence(
                new Sequence(
                    new Condition(IsTargetInSameRoom),
                    new BehaviourTrees.Action(LookAtTarget),
                    new BehaviourTrees.Action(Shoot)
                    ),
                new BehaviourTrees.Action(MoveToTarget)
                )
        );
    }

    private BehaviourState LookAtTarget()
    {
        if (!_entity.hasTurn)
        {
            _entity.AddTurn(
                new Vector2(_entity.target.Target.view.Value.transform.position.x,
                    _entity.target.Target.view.Value.transform.position.z), Time.deltaTime);
        }
        else
        {
            _entity.ReplaceTurn(
                new Vector2(_entity.target.Target.view.Value.transform.position.x,
                    _entity.target.Target.view.Value.transform.position.z), Time.deltaTime);
        }

        return BehaviourState.Success;
    }

    private bool HasNoTargetSelected()
    {
        if (!_entity.hasTarget)
        {
            return true;
        }
        return false;
    }

    private BehaviourState MoveToTarget()
    {
        return BehaviourState.Success;
    }

    private BehaviourState SelectTarget()
    {
        _entity.AddTarget(_context.GetEntityWithName("Player"));
        return BehaviourState.Success;
    }


    private bool IsTargetInSameRoom()
    {
        if (_entity.target.Target.hasRoomId)
        {
            if (_entity.roomId.Value == _entity.target.Target.roomId.Value)
            {
                return true;
            }            
        }
        return false;
    }

    private BehaviourState Shoot()
    {
        _entity.hasFired = false;
        _entity.hasFired = true;
        return BehaviourState.Success;
    }

    private void Update()
    {
        if (_root == null)
            return;
        _root.Tick();
    }
}
