﻿using System.Collections.Generic;

namespace BehaviourTrees
{
    public class Sequence : CompositeNode
    {
        public Sequence(params INode[] childNodes) : base(new List<INode>(childNodes))
        {
        }

        public override BehaviourState Tick()
        {
            foreach (var childNode in ChildNodes)
            {
                BehaviourState state = childNode.Tick();

                switch (state)
                {
                    case BehaviourState.Failure:
                        return state;

                    case BehaviourState.Running:
                        return state;
                }
            }
            return BehaviourState.Success;
        }
    }
}
