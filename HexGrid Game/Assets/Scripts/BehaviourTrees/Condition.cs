﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BehaviourTrees
{
    public delegate bool ConditionDelegate();

    public class Condition : INode
    {
        private ConditionDelegate _conditionDelegate;

        public Condition(ConditionDelegate conditionDelegate)
        {
            _conditionDelegate = conditionDelegate;
        }


        public BehaviourState Tick()
        {
            if (_conditionDelegate != null &&  _conditionDelegate())
                return BehaviourState.Success;

            return BehaviourState.Failure;
        }
    }
}
