﻿using System;

namespace Models
{
    public class SeedChangedEventArgs : EventArgs
    {
        public string Seed { get; private set; }
        public SeedChangedEventArgs(string seed)
        {
            Seed = seed;
        }
    }
    public class SeedModel
    {
        public event EventHandler<SeedChangedEventArgs> OnSeedChanged;

        private string _seed;
        public string Seed
        {
            get { return _seed; }
            set
            {
                _seed = value;
                FireOnSeedChanged(_seed);
            }
        }

        private void FireOnSeedChanged(string value)
        {
            if (OnSeedChanged != null)
            {
                OnSeedChanged(this, new SeedChangedEventArgs(value));
            }
        }
    }
}
