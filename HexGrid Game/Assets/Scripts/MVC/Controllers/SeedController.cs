﻿using System;
using System.Diagnostics;
using Models;
using Views;
using Views.Impl;

namespace Controllers
{
    public class SeedController
    {
        public SeedModel SeedView { private get; set; }

        public ISeedView Seed { private get; set; }

        public void Init()
        {
#if DEBUG
            Debug.Assert(SeedView != null);
            Debug.Assert(Seed != null);
#endif

            Seed.Seed = SeedView.Seed;

            //models changes
            SeedView.OnSeedChanged += ModelSeedChanged;

            //view change
            Seed.OnSeedChanged += ViewChanged;

        }

        private void ViewChanged(object sender, Views.SeedChangedEventArgs eventArgs)
        {
            SeedView.Seed = eventArgs.Seed;
        }

        private void ModelSeedChanged(object sender, Models.SeedChangedEventArgs eventArgs)
        {
            SetValue(eventArgs.Seed);
        }

        private void SetValue(string seed)
        {
            Seed.Seed = seed;
        }
    }
}
