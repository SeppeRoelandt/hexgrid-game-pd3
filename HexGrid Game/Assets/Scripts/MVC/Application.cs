﻿using Controllers;
using Models;
using UnityEngine;
using UnityEngine.Assertions;
using Views;
using Views.Impl;

public class Application : MonoBehaviour
{

    [SerializeField]
    private GameObject _seedView1;

    [SerializeField]
    private GameObject _seedView2;

    private SeedModel _seed;
    private SeedController _SeedCtrl1;
    private SeedController _SeedCtrl2;

    void Start()
    {
#if DEBUG
        Assert.IsNotNull(_seedView1);
        Assert.IsNotNull(_seedView1.GetComponent<ISeedView>());
#endif
        _seed = new SeedModel();
        _seed.Seed = "Seed";
        _SeedCtrl1 = new SeedController();
        _SeedCtrl1.SeedView = _seed;
        _SeedCtrl1.Seed = _seedView1.GetComponent<ISeedView>();

        _SeedCtrl1.Init();

        _SeedCtrl2 = new SeedController();
        _SeedCtrl2.SeedView = _seed;
        _SeedCtrl2.Seed = _seedView2.GetComponent<ISeedView>();

        _SeedCtrl2.Init();
    }
}
