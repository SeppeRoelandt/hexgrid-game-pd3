﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Views
{
    public class SeedChangedEventArgs : EventArgs
    {
        public string Seed { get; private set; }
        public SeedChangedEventArgs(string seed)
        {
            Seed = seed;
        }
    }

    public interface ISeedView
    {
        event EventHandler<SeedChangedEventArgs> OnSeedChanged;
        string Seed { set; }
    }

    namespace Impl
    {
        public class SeedView : MonoBehaviour, ISeedView
        {
#pragma warning disable 649
            [SerializeField]
            private InputField _seed;
#pragma warning restore 649

            public event EventHandler<SeedChangedEventArgs> OnSeedChanged;

            private void Start()
            {
#if DEBUG
                Assert.IsNotNull(_seed);

#endif
                _seed.onValueChanged.AddListener(FireOnValueChanged);
            }

            private void FireOnValueChanged(string value)
            {
                if (OnSeedChanged != null)
                {
                    OnSeedChanged(this, new SeedChangedEventArgs((string)value));
                }
            }

            public string Seed
            {
                get { return _seed.text; }
                set { _seed.text = value; }
            }


        }
    }


}
