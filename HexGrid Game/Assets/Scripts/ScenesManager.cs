﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{

    public GameObject StartCanvas;
    public void StartGame()
    {
        StartCanvas.SetActive(false);
        //generate Maze
        gameObject.GetComponent<MazeBehaviour>().Generate();
    }

    public void QuitGame()
    {
        Debug.Log("bye bye game");
        UnityEngine.Application.Quit();
    }
}
