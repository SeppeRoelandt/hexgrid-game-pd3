﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas.Unity;

public class GameWon : MonoBehaviour {

    private GameObject _won;

    void Start()
    {
        _won = GameObject.FindGameObjectWithTag("Won");
        _won.SetActive(false);
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _won.SetActive(true);
           GameEntity entity = other.gameObject.GetEntityLink().entity as GameEntity;
            entity.isPlayer = false;
        }
    }
}
